# locale-gen en_US.UTF-8
# dpkg-reconfigure locales

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

export VISUAL=vim
export EDITOR=$VISUAL
export SHELL=bash

if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

# Colors
if [[ $TERM == *256color ]]; then
    eval `dircolors ~/.dircolors`
    alias ls="ls --color"
    alias grep='grep --color=auto'
fi

# Set prompt
PS1="\[\e]0;\w\a\]\n\[\e[32m\]\u@\h \[\e[33m\]\w\[\e[0m\]\n$ "

# Disable XON/XOFF flow control, to avoid collision of Ctrl-s (useful with Ctrl-r)
stty -ixon

# Increase history
export HISTFILESIZE=5000

# Configure custom path of pkg-config
if [ -d /usr/local/lib/pkgconfig ]; then
    export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/
fi

# Git aliases
alias gst='git status -s'

# Just in time debugger
export CYGWIN="$CYGWIN error_start=gdb -nw %1 %2"

# Hugo quick log add
function journal {
    filename=notes/`date +%Y-%m-%d`.md
    pushd ~/blog
    hugo new $filename
    $VISUAL content/$filename
    popd
}

# Setup luarocks
if [ -x "$(command -v luarocks)" ]; then
    eval `luarocks path`
fi
