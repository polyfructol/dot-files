" vim:fdm=marker
set nocompatible

" Natively load plugins
set packpath+=~/.vim/pack/

" Various options {{{
set noerrorbells visualbell t_vb= " Disable error bell
set shortmess=I " Avoid intro message
set backspace=2 " Make backspace work like other apps
set ttimeoutlen=0 " No delay when switching from INSERT to NORMAL mode
set ai " Auto-indent new lines
" }}}

" Search {{{
set incsearch " Incremental search
set hlsearch " Highlight search
nnoremap <silent> <C-l> :nohlsearch<CR><C-l> " Clear last search with Ctrl-l
set ignorecase " \C to force case sensitive
" }}}

" Indentation {{{
if has("autocmd")
  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on
endif
set expandtab " Change tab to space
set tabstop=4 " Nb space to replace a tab
set softtabstop=4 " Backspace will automatically delete 4 spaces if its a tabulation
set shiftwidth=4 " Space inserted when shift right (>) is pressed
" }}}

" Text display {{{
set nowrap " No word-wrap
set number " Show line numbers
" set relativenumber " Relative line numbers
" Quickly switch relative number
nnoremap <Leader>n :set relativenumber!<CR>

" Font and syntax highlighting
syntax on
set guifont=Consolas:h10:cANSI
set background=dark
colorscheme solarized
if &term == "screen" " Needed in tmux to support 256 colors
    set t_Co=256
endif
" }}}

" Mouse {{{ -------------------------------------------------------------
" Enable mouse use in all modes
set mouse=a

" Set this to the name of your terminal that supports mouse codes.
" sgr do not have 223 columns limit
if has("mouse_sgr")
    set ttymouse=sgr
else
    set ttymouse=xterm2
end

" Double click will highlight every occurence of the word under cursor
nnoremap <silent> <2-LeftMouse> :let @/='\V\<'.escape(expand('<cword>'), '\').'\>'<cr>:set hls<cr>
" }}}

" Auto completion {{{
set completeopt=longest,menuone
" <CR> select highlited menu item
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" <C-n> display completion menu without completing with first match
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
set tags+=system_tags " Used to load 2 tags file (one for projet, one for system)
" }}}

" Netrw config {{{
" Tips: Use :Lexplore to create a left split with netrw
" Tips: Use <C-w> t to move to top-left split
let g:netrw_browse_split=4 " Open file in last split
" let g:netrw_liststyle=3 " Tree style
let g:netrw_winsize=-28 " Netrw split default size
nnoremap <leader>e :Lexplore<CR>
" }}}

" Custom functions {{{
" Remove trailing whitespace on save
function! TrimWhiteSpace()
    %s/\s\+$//e
    ''
endfunction

command MyTrimWhiteSpace %s/\s\+$//e
" Open help in current split (:Help) 
function! OpenHelpInCurrentWindow(topic)
    view $VIMRUNTIME/doc/help.txt
    setl filetype=help
    setl buftype=help
    setl nomodifiable
    exe 'keepjumps help ' . a:topic
endfunction

command! -nargs=? -complete=help Help call OpenHelpInCurrentWindow(<q-args>)
" }}}

" Folding options {{{ ---------------------------------------------------------
"  Tips: :%foldc to close one level of fold
"  Tips: za to switch, zc close, zo open, zM close all, zR open all
set foldmethod=indent
set foldlevelstart=20 " Avoid auto-fold when opening files
" }}}

" vim-airline {{{ -------------------------------------------------------------
" Enable the list of buffers
"let g:airline#extensions#tabline#enabled = 1
" Display special powerline symbol (font must support it)
let g:airline_powerline_fonts = 1
" Display status line all the time
set laststatus=2
" }}}

" ctrlp config {{{ -------------------------------------------------------------
" display hidden files
let g:ctrlp_show_hidden = 1
let g:ctrlp_cmd = 'CtrlPBuffer'
" }}}

