########## Variables

dir=~/dot-files        # dotfiles directory
olddir=~/dot-files_old # old dotfiles backup directory
softlinks=".bashrc .vimrc .vim .tmux.conf .minttyrc .mintty .inputrc .dircolors .spacemacs .Xresources .Xresources.d"  # list of files/folders to symlink in homedir
hardlinks=".gitconfig"  # list of files/folders to hardlink in homedir

##########

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create links 
for file in $softlinks; do
    if [ -f ~/$file ] || [ -d ~/$file ]; then
        echo "Moving existing $file from ~ to $olddir"
        mv ~/$file $olddir
    fi
    echo "Creating softlink to $file in home directory."
    ln -s $dir/$file ~/$file
done

for file in $hardlinks; do
    if [ -f ~/$file ] || [ -d ~/$file ]; then
        echo "Moving existing $file from ~ to $olddir"
        mv ~/$file $olddir
    fi
    echo "Creating hardlink (issue with vscode with symlinks) to $file in home directory."
    ln $dir/$file ~/$file
done

source ~/.bashrc
